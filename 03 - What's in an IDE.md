# What's an Integrated Development Environment?

An Integrated Development Environment is hard to type, first of all, so the name gets shortened to IDE usually. But basically, the IDE is the set of software that you run on your computer that lets you take code and copmile it and get it loaded onto your processor.

The "integrated" part is because it's actually a bunch of different tools that are combined into one common user interface, for your convenience. There's the editor for typing your code in, there's the project manager which keeps track of which files are part of the project, there's the compiler for taking those code files and turning them into machine instructions, there's the linker for taking all the different machine instruction files and combining them into one binary, there's the programmer for taking the binary and loading it into the processor... And that's just the "major" pieces, there's plenty more pieces hidden away.

But in short, the IDE is the application which takes your code and makes it run on your processor.

There's are a lot of different IDEs out there, usually at least 3 for each major type of processor out there, with some getting a whole lot more than others. The Arduino program is an IDE, though it's very very minimal with it's features.

For STM32, there's five different IDEs that I've personally tried, and at least a dozen more that I haven't, all with varying success. But the one I'm going to use for this tutorial is the one provided by ST Microelectronics themselves, [STM32CubeIDE](https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-ides/stm32cubeide.html). This IDE is technically brand new, but it's mostly a rebranding of another IDE that they bought and made some changes to and then released under a new name, so it's fairly solid already. I'm on Windows generally, so that's what I'll be using, but they provide packages for Mac OS X and several flavors of Linux, too, so hopefully those will work just as well.

Get that downloaded and installed, and then we'll start taking a look around.

# Starting up STM32CubeIDE

When you first start up STM32CubeIDE, it will ask you to select a directory as workspace. This workspace is where it will store files by default, so we need to make sure we know where it is in case we want to look at something outside of the IDE. By default, it tries to put it in your User folder in an STM32CubeIDE folder, which is a pretty good place for it, so check the "Use this as the default and do not ask again" checkbox, and click "Launch".

# Setting up a project

After that, it will take you to a window that looks like [this](https://drive.google.com/file/d/1teeTsAs94Gh2FBhOaXt4fJFMkWIq8R5C/view?usp=drivesdk). We don't have any existing projects, so we want to make a new one, so click that "Start new STM32 project" button. It'll take a minute to download the list of STM32 processors from ST's site, then come to [this](https://drive.google.com/file/d/1BkO9bdSNecMcIQMrEfG_M_9PuBWAgE7b/view?usp=drivesdk) screen. This is where you get to pick which processor this project is for. We know that the Blue Pill board has an STM32F103C8 , so type that into the search box in the top left, then select the only result in the bottom right panel and click Next. This brings us to [this](https://drive.google.com/file/d/103BGAXockWE49-JkmdTavBneGvuDi_tZ/view?usp=drivesdk) screen. All of the options on this screen are fine, we just need to pick a name. Pick a name (I went with "First Project"), then click Finish.

It will take a while to think and download some more things from ST's site, then go to [this](https://drive.google.com/file/d/1Rf8BbeMEprvvpT1DvuqUjcJTU30n8e4j/view?usp=drivesdk) fancy looking screen. This is where you'll configure the hardware peripherals and the code libraries to support those peripherals. We're only going to do a little bit of configuration for now, but we'll be back here later.

(NOTE: Somewhere along the way here, I missed exactly where, the IDE will ask you a question about "opening a view" or "opening a perspective" or "switch to a view" or "switching perspectives". Say yes when it does.)

# Peripheral Configuration

For now, expand the System Core section on the left, then click RCC. This is the "Reset and Clock Control" configuration section, and is mostly used to turn on various oscillators that can be used for timing sources. The Blue Pill board that we're using includes both an 8MHz crystal and a 32.768KHz crystal, which are hooked up to the HSE(High Speed External) and LSE(Low Speed External) clock peripherals, respectively. The 8MHz crystal is usually what you'll use for running the processor core and peripherals, while the 32.768KHz crystal is usually used for precise time keeping(In fact, if you have a digital watch, it most likely has a 32.768KHz crystal in it somewhere). For now, just set both the HSE dropdown menu and the LSE dropdown menu to "Crystal/Ceramic Resonator".

We also want to make sure our programmer stays connected while we're running our program, so click the "SYS" entry next to "RCC", and select "Serial Wire" in the Debug dropdown menu. We can't really use any of the others as the Blue Pill board only brings out the pins for Serial Wire Debugging, so let's not worry about them for now.

You've probably noticed that some of the pins on the chip model on the right have been turning green as you've done this. Those are the pins that are needed by the peripherals you've turned on. If you play with the different options in the Debug dropdown, you'll see that different setups require different numbers of pins in different arrangements. This graphical representation of which pins are used for what is actually a very powerful feature that ST has going for them. Not many vendors have previously done this, though a few have started recently. Anyways, make sure that Debug is set to "Serial Wire", and let's move on.

Next, we're going to set one of the GPIO pins to be an output pin. The Blue Pill board has an LED hooked up to pin PC13(GPIO Port C, pin 13), so we'll use that one so we can easily see if it's working. This one we're going to do in a slightly different way. Since we know which pin the LED is hooked up to, we can just click that pin on the graphical view of the chip(It's in the top left, right under the VBAT pin) and get a menu of all the functions that pin can have associated with it. We want it to be a basic output pin, so select "GPIO_Output" from that menu. It turns green, and we're done. We can configure all of our GPIO pins this way. Setting up peripherals is usually easier through the menus, but plain old GPIO pins you're better off clicking it directly.

# Clock Configuration

Next we want to go to the "Clock Configuration" tab along the top. This brings us to a [fairly intimidating screen](https://drive.google.com/file/d/1bCmAxat8HA6T1SrgOXgaRuP-Lryrbls1/view?usp=drivesdk)... This is actually showing you how the various clock signals get routed through the chip to various things that need it. The main things we're concerned with are the boxes that have blue text under them listing a maximum speed. Because we're just playing around and aren't concerned about power consumption, we're going to try and get those numbers as close to the maximum as possible.

Right in the middle of the upper block, everything narrows back down to the "SYSCLK (MHz)" box, then splits back out again. To the right of that, HCLK has a limit of 72 MHz. The HCLK is what the actual processor core runs on, so having that be higher means our program will run faster, but we can't go above 72MHz without causing problems. So we want to get HCLK to 72 MHz.

Just to the left of SYSCLK, there's the "System Clock Mux" block, which lets us pick from a few different sources for SYSCLK. By default, it uses HSI(High Speed Internal), which is an 8MHz resistor/capacitor oscillator which isn't very accurate, but also doesn't require any external components. We do have that 8MHz crystal on the HSE pins, but that's not actually any faster, just more accurate. But what's that PLLCLK option?

A PLL is a "Phase-Locked Loop", which probably doesn't help explain anything, but in this case, you can just think of is as a "frequency multiplier". Follow that PLLCLK option's line back to get to the light blue box, and these are the options for the PLL. The box on the left of the light blue background is the "input frequency", and the box on the right is how much you want to multiply the input frequency by. Just to the left of the light blue box is another Mux, this one picking the source of the clock input to the PLL. By default this also use the HSI oscillator.

But wait, why does the "input frequency" show 4 MHz, when the HSI oscillator is an 8 MHz oscillator? Just before the PLL Source Mux, there's a divide-by-2 block on the HSI input. This is done to try to help reduce some of the inaccuracy of the resistor/capacitor oscillator. We can make up the lost speed with the PLL, so we gain a little bit of accuracy without losing any speed. But again, we have the HSE crystal, which is even more accurate, so set the PLL Source Mux to HSE. Now the PLL input speed box shows 8 MHz, as expected. This didn't actually increase our SYSCLK speed, though, but that's because the System Clock Mux is still set to HSI. Go ahead and change it to PLLCLK.

Hooray, now we're up to 16 MHz on the SYSCLK! But we want 72 MHz, so go back to the PLL Settings and start increasing the multiplier, one at a time. HSE x9 gets us up to 72 MHz in our SYSCLK, but if you were paying attention, the "APB1 Prescaler" and "APB1 peripheral clocks (MHz)" boxes both turned an ugly red when we got to x5. This is because PCLK1 is limited to 36MHz, which you can see between those two ugly red boxes. But we can fix this, because the APB1 Prescaler is a configurable division. 72/2 happens to be 36, so set AP1 Prescaler to /2, and everything is happy again.

There's one other thing that you want to do when using the PLL as your SYSCLK source, which is to cliuck the "Enable CSS" button right below the System Clock Mux. This turns on an extra bit of safety hardware that will switch the system clock back to the HSI oscillator in the very rare event that your HSE oscillator fails. This happens pretty much seamlessly, so you might as well turn it on just in case.

With that, we're done configuring the peripherals. Click the Save button in the toolbar, and the IDE will pop up asking "Do you want to generate Code?". This is where the graphical configuration we just did gets turned into actual code to run on the processor, so click Yes. I don't recommend checking "Remember my decision" on this one, as it's a good time to stop and re-think about what you're doing and whether it's actually the right thing. In this case, this is the first time we're generating code, so there's not much to think about.

And now we've got our project set up.

# Exploring the project

So over on the left of the window is the "Project Explorer" panel. This is where you'll find all the files that are in your project, whether they're ones you've made, or ones that the graphical peripheral configuration generated for you, or system support files that are automatically included for you.

The "Includes" folder you can generally ignore, as it's where the IDE sticks everything that it went and found automatically.

The "Drivers" folder is where ST's code for running all the peripherals lives. Generally you won't need to touch this one, either.

The "Src" folder is where you'll put your code files. If you look right now, you'll see main.c and a handful of other files. We'll be back here in a minute.

The "Startup" folder only contains one file, which is a special file that is used to get the chip ready to start your C program. Mostly what it does is makes sure that your global variables are initialized, and then it calls your main() function.

The "Inc" folder is for your header files. Technically all of these files could be in the Src folder, I don't know why people tend to split them apart...

There's also two files that aren't in a folder, "First Project.ioc", which is the file that the graphical peripheral configuration tool stores things in, and a .ld file, which is used by the linker to figure out how to stick things together for this particular processor. If you ever want to get back to the graphical peripheral configuration, double-click the .ioc file.

# main.c

Now, if you open up main.c (In the Src folder), you'll see that there's quite a few lines of stuff in here already, and a lot of those lines say something about "USER CODE", with matching "BEGIN" and "END" lines. These are an unfortunate downside to the graphical configuration. If you ever want to make changes to the peripheral configuration with the graphical tool, you need to make sure that all of your code in main.c falls in between a USER CODE BEGIN and it's matching USER CODE END. Annoying, yes, but we can work with this.

Here's a cleaned up version for a quick reference.

```C

#include "main.h"

/* Private includes ----------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);

/* Private user code ---------------------------------------------------------*/

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();

  /* Infinite loop */
  while (1)
  {

  }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* User can add his own implementation to report the HAL error return state */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
}
#endif /* USE_FULL_ASSERT */

```

main() actually isn't too different from the one we dug up back on Arduino. It call a few initialization functions, then goes into an infinite loop. It doesn't have the setup() or loop() calls, but that wouldn't be hard to add.

Most of the rest of the code in here is setting up the hardware to match your configuration in the graphical tool. SystemClock_Config() sets up the oscillators and the PLL, and MX_GPIO_Init() sets up the pin that the LED is on.

The two functions at the end, Error_Handler() and assert_failed(), we're not going to worry about right now. Just ignore them.

# Compiling

So we have a project. We don't have any of our code in it, yet, but there is some setup code, so let's just make sure that the compiler does it's thing. Up on the toolbar, there should be a little hammer icon, which if you hover over it will say "Build 'Debug' for project 'First Project'". This is the button you'll be using to compile your code(Similar to the Verify button on Arduino). If you click it, down towards the bottom of the screen you should see a Console tab that starts scrolling a bunch of text, which should eventually result in a blue "Build Finished. 0 errors, 0 warnings." If it doesn't, something's gone wrong and you should try and get in touch so I can figure out what happened.

Now, we know that the compiler works. I think that's enough for now. Next time, hopefully your programmer and development board have arrived, and we can actually start getting code running on it.