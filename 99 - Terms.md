# Terms #

## Numerical Notation Prefixes ##
* `0b` : Binary. Base 2. See https://en.wikipedia.org/wiki/Binary_number .
* `0x` : Hexadecimal. Base 16. See https://en.wikipedia.org/wiki/Hexadecimal .
* Without a prefix, a number is presumed to be decimal, or base 10. See https://en.wikipedia.org/wiki/Decimal .

Computers actually do their work in binary, but the human-readable representation of binary takes up a lot of space, so it will often be condensed to hexadecimal, where 4 binary bits can be shown in a single hexadecimal character.

All of these can be converted to any other, but some things are easier to think of or work with or show the effects of in specific notations, so they'll be used as needed.

## Operators, basic ##
* `=` : Assignment. Sets the thing to the left equal to the thing on the right.
* `*` : Multiplication. Multiplies the thing to the left by the thing to the right.
* `/` : Division. Divides the thing on the left by the thing to the right.
* `+` : Addition. Adds the thing on the right to the thing on the left.
* `-` : Subtraction. Subtracts the thing on the right from the thing on the left.

## Operators, bitwise ##
* `>>` : Right shift. Takes the thing to the left and shifts it right by the value to the right bits. Mostly used for binary data(0b0010 >> 1 equals 0b0001, for example), or as a shortcut to divide by 2 (4 >> 1 equals 2).
* `<<` : Left shift. Takes the thing to the left and shifts it left by the value to the right bits. Mostly used for binary data, or as a shortcut to multiply by 2.
* `&` : Bitwise AND. I'm gonna refer to Wikipedia for this one. https://en.wikipedia.org/wiki/Bitwise_operation#AND
* `|` : Bitwise OR. Also Wikipedia. https://en.wikipedia.org/wiki/Bitwise_operation#OR
* `^` : Bitwise XOR. Wikipedia. https://en.wikipedia.org/wiki/Bitwise_operation#XOR
* `~` : Bitwise NOT. Wiki. https://en.wikipedia.org/wiki/Bitwise_operation#NOT

## Operators, shorthand ##
* `+=` : Adds the thing to the right to the variable on the left.
* `-=` : Subtracts the thing on the right from the variable on the left.
* `*=` : Multiplication. Multiply the variable on the left by the thing on the right.
* `/=` : Division. Divide the variable on the left by the thing on the right.
* `&=` : AND. AND the variable on the left with the thing on the right.
* `|=` : OR. OR the variable on the left with the thing on the right.
* `^=` : XOR. XOR the variable on the left with the thing on the right.

## Operators, logical ##
* `true` : In general, a nonzero value, but if used by name, a value of 1.
    * Note that for the purposes of comparisons, the name "true" has an exact value of 1, so `if( (IO_PIN_INPUT & 0x03) == true)){}` will NEVER evaluate to true, because the pin we're looking at would result in values of either `0` or `4`, but never a value of `1`.
	* `if( (IO_PIN_INPUT & PIN3) ){}` will evaluate to true or false appropriately, because a nonzero value is _implicitly_ true, but not _exactly equal to_ `true`.
* `false` : A zero value.
* `==` : Equality. Checks whether the thing on the left and the thing on the right have the same value, and collapses itself and the thing to the left/right down to a single "true" or "false".
* `!=` : Inequality. Checks whether the thing on the left and the thing on the rigbht have different values, and if so, collapses itself and the thing to the left/right down to a "true", otherwise "false".
* `!` : Logical negation. If the thing that follows is "true", it will read as "false", and vice-versa.
* `||` : Logical OR. If either the thing on the left or the thing on the right evaluates as "true", this collapses all three to "true", otherwise "false".
* `&&` : Logical AND. If both the thing on the left and the right evaluate as "true", this collapses all three to "true", otherwise "false".

## Operators, scope ##
* `()` : Inside expressions, parentheses can be used to explicitly state the order in which you want things to happen, from inner-most to outer-most. Without parentheses, expressions generally get evaluated in order according to the [Order of Operations](https://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#Operator_precedence) , but there are cases where compilers just do weird things, so it's better to be explicit than implicit.
* `{}` : Used for grouping a block of code together. Commonly seen after conditionals and loops, to mark what is affected by the loop/conditionals, but you can use these independently, as well, just to subdivide your code into logical units, if needed.

## Type fundamentals ##
* Integer : A number with no decimal points. 2, 4, 5, etc.
* Decimal : A number with a decimal point. 1.3, 2.5, 3.0, etc.
* Signed : A number that can be positive or negative.
* Unsigned : A number that can only be positive.
* Range : The minimum and maximum values that can be stored in a variable.

## Types, general ##
* `Char` : A signed integer. The exact range and size varies depending on the compiler/processor you're on, but is generally -128 to +127 , and takes up a single byte of memory.
* `Short` : A signed integer. The exact range and size varies depending on the compiler/processor you're on, but is generally -32,768 to 32,767 , and takes up 2 bytes of memory.
* `Int` : A signed integer. The exact range and size varies depending on the compiler/processor you're on, but is generally -2,147,483,648 to 2,147,483,647 , and takes up 4 bytes of memory.
* `Float` : A signed decimal. The exact size varies depending on the compiler/processor you're on, but is generally 4 bytes.
* `Double` : A signed decimal. The exact size varies depending on the compiler/processor you're on, but is generally 8 bytes. Twice as precise as a Float!
* `Long` : A signed decimal. The exact size varies depending on the compiler/processor you're on, but is generally 12 bytes. More precise than a Double!

## Type Modifiers ##
* `unsigned` : Can be used before a general type to make the general type unsigned. "unsigned short" will generally get you a 2-byte variable with a range of 0 to 65535.
* `signed` : Can be used to explicitly state that you want a signed variable.
* `volatile` : Marks the follow variable as "rapidly changing", or something that could change without your code explicitly setting it. Mark any variables that get modified in interrupts as volatile!
* `const` : Marks a variable as constant. Your code will be unable to change it from it's initial value. Useful for configuration stuff, or assigning names to "magic" numbers.

## Types, standardized ##
* `uint8_t` : Unsigned 8-bit integer. Single byte, range 0 to 255.
* `int8_t` : Signed 8-bit integer. Single byte, range -128 to +127.
* `uint16_t` : Unsigned 16-bit integer. Two bytes, range 0 to 65535.
* `int16_t` : Signed 16-bit integer. Two bytes, range -32,768 to 32,767.
* `uint32_t` : Unsigned 32-bit integer. Four bytes, range 0 to 4,294,967,295.
* `int32_t` : Signed 32-bit integer. Four bytes, range -2,147,483,648 to 2,147,483,647.

## Types, custom ##
* `typedef` : Creates a new variable type. So if you want a new type named "SuperAwesomeByte", you can do `typedef uint8_t SuperAwesomeByte`, and then at any point after that, you can declare a variable with the type of `SuperAwesomeByte`, which is actually a uint8_t.
* `enum` : Creates a list of values that have names associated with them. So you can have, for example, an enum for days of the week. Combine with `typedef` for maximum effect.

### Enum example ###
```C
typedef enum {
	WEEKDAY_MONDAY = 0,
	WEEKDAY_TUESDAY,
	//WEEKDAY_WEDNESDAY,	//Wednesday is cancelled due to scheduling issues.
	WEEKDAY_THURSDAY,
	WEEKDAY_FRIDAY,
	WEEKDAY_SATURDAY,
	WEEKDAY_SUNDAY,
	
	WEEKDAY_INVALID = 0xFF,
} WEEKDAYS_e;

WEEKDAYS_e currentWeekday = WEEKDAY_TUESDAY;
```

Note that you can assign specific numerical values to names, but if you don't, the value will automatically increment from the previous value.

## Types, complex ##
* `[]` : Used as a type modifier. Creates an array, or a set of multiples, of the preceeding variable type. Put a number between the square brackets to define the size of the array, and then you can access each individual piece of the array by indexing into it using `variablename[index]`.
    * Note, the SIZE of the array starts at 1, but the INDEX starts at 0. The SIZE is an amount(And having 0 of a variable is pretty useless), the INDEX is the "offset" from the first one to the one you want.
* `struct` : Creates a "structure" containing a variety of variables. Use this to organize things that need to stay together, together. Combine with `typedef` for maximum effect.
* `union` : Creates a set of "overlapped" variables. Each variable in the overlap is actually using the same piece of memory, so writing to one variable will affect the others, as well. Generally used for working with binary data that you're receiving from some communication protocol, where the receiving code doesn't actually know what the data is, just that it's not done receiving it yet.

### Struct example ###
```C
typedef struct {
	uint8_t characters[30];	//Arbitrarily chosen limit of 30 characters in a string.
	uint8_t usedCharacters;
} MyString_t;

MyString_t myString;

void main(void) {
	myString.characters[0] = "H";
	myString.characters[1] = "e";
	myString.characters[2] = "l";
	myString.characters[3] = "l";
	myString.characters[4] = "o";
	myString.characters[5] = " ";
	myString.characters[6] = "W";
	myString.characters[7] = "o";
	myString.characters[8] = "r";
	myString.characters[9] = "l";
	myString.characters[10] = "d";
	myString.characters[11] = "!";
	
	myString.usedCharacters = 12;
}
```

### Union example ###
```C
typedef struct {
	uint8_t packetType;
	uint8_t destinationAddress;
	uint8_t sourceAddress;
	uint8_t payload[4];
	uint8_t checksum;
} ActualPacketStructure_t;

typedef struct {
	union {
		uint8_t rawData[8];	//Packets are always 8 bytes
		ActualPacketStructure_t packet;
	};
} ReceivedPacket_t;

ReceivedPacket_t receivedPacket;
```

The receiving code would write data into the receivedPacket.rawData array, while the actual code to deal with packets would use the receivedPacket.packet structure to get to specific fields by name.

## Conditionals ##
* `if() {}` : If the expression inside the parentheses evaluates to true, the code inside the curly braces will execute.
* `switch() {}` : This one needs an example.

### Switch example ###
```C
switch( randomNumberHere ) {
    case 0:
		//The number is 0!
		break;
	case 1:
		//The number is 1
		break;
	case 2:
		//The number is 2
		//But we actually want to execute the code that is in case 3...
		//So we're not going to put a "break" here.
		//This is called "falling through a case".
	case 3:
		//The number is 3
		break;
	default:
		//The number is something else
		break;
}
```

A Switch lets you run different blocks of code based on the value of an expression.

The value of the expression inside the parentheses is found, then compared with the value to the right of each case within the curly braces.

If a match is found, the code btween the matching case's colon and the next "break" is executed.

If no match is found, but the curly braces contain a "default" case, then the code between the default's colon and the next "break" is executed.

## Loops ##
* `while() {}` : The expression inside the parentheses is evaluated, and if true, the code inside the curly braces is executed. Once that code finishes, the expression will be evaluated again, and if true, the code will be run again. This repeats until the expression evaluates to false.
* `do{} while()` : The code inside the curly braces is executed, then the expression inside the parentheses is evaluated. If true, the code will be executed again, then the expression evaluated again. This repeats until the expression evaluates to false. Very similar to a while() {}, but the code is guaranteed to run at least once.
* `for(;;) {}` : This one needs an example.

### For example###
```C
for(int i = 0;i < 5;i++) {
	//Not done yet
}
```

A For loop lets you run code a specific number of times, and puts all of the "control" of that loop right at the beginning of the loop.

The code between the left parenthesis and the first semicolon is executed (int i = 0), then the expression between the first and second semicolons is evalutated (i < 5).

If the expression is true, the code in the curly braces is executed, then the code between the second semicolon and the right parenthesis is executed (i++).

Then the expression is evaluated again, and if true, the code in the curly braces runs again, then the code btween the second semicolon and the right parenthesis is executed, and the cycle repeats until the expression evaluates to false.
