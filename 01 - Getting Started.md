# Introduction

Hokay. So you want to follow along with my code, but can't figure out what the heck I'm doing sometimes?

That's understandable, I've been programming for... 14 years now? Going on 15? Either way, more than half of my life at this point. Yes, I started programming when I was 13. It was a rough start, and I didn't really take off until I was 16-ish, but I did poke around in those three years in between, so I still count them. I'm not saying this to brag, but just to show that there's good reason my code may look alien at first.

The field of programming I work in is called "Embedded Programming", which is... Difficult to describe. https://www.techopedia.com/definition/29945/embedded-programming for an example of the typical definition. But basically, it boils down to that I don't get the niceties of an operating system like Windows to do some of the heavy lifting for me. What this usually means, to me, at least, is that the only code I have to depend on is my own, I don't usually get to use a library that somebody else has written.

This isn't to say that embedded programming is a completely solo affair, just that additional care needs to be taken when writing code to make sure that it can be used in multiple projects, not just the one you're writing right now. This leads to some slightly odd thought processes, but usually they're for the better of both the project I'm working on right now, and the next one I'll be working on in the future.

But I'll get into that more later.

For now, let's get into some setup work.

# Prerequisites

I'm going to assume that you have some experience with Arduino programming. If you don't, go read through some of the tutorial stuff on their website and then come back. https://www.arduino.cc/en/Tutorial/BuiltInExamples Go through at least the "Basics" section, and the "Button" part of the "Digital" section.

I'm also going to be using some Arduino code here for the first few sections. "But Izzy," I hear you say, "aren't you the guy who hates Arduino with a fiery, fiery passion?" Yes. Yes I am. But just saying "don't use Arduino" doesn't really help, without explaining why.

Also, if you see anything odd that you don't recognize, check [here](https://bitbucket.org/izzy84075/tutorials/src/master/99%20-%20Terms.md) for a list of terms and keywords that get used. If you find something that's not in there, let me know, and I'll try to add it.

So let's look back at that Blink code.

# Make It Blink

```C

/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}

```

Not too bad, really. setup() initializes the LED pin to an output, and loop() turns the LED on, waits a second, turns the LED off, then waits another second, then repeats. For what it's doing, this code is fine.

Let's take a look at the Button code.

# Press The Button!

```C

/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital pin 13,
  when pressing a pushbutton attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground
  - pushbutton attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  - Note: on most Arduinos there is already an LED on the board
    attached to pin 13.

  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Button
*/

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}

```

This code is... Not great, but for an example, it's fine. setup() sets the LED pin to an output, and the button pin to an input. Then, while the button is pressed, it turns the LED on.

Now, let's try and combine these two examples. When the button is pressed, you want the LED to turn on for a second, then turn off. You'll probably come up with something like this:

# Press To See

```C

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(1000);                       // wait for a second
    digitalWrite(ledPin, LOW);    // turn the LED off by making the voltage LOW
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}

```

Which is okay, on first blush. But what happens if you press the button again while the LED is already on? As a user, you would expect the LED to stay on for 1 second from the last time you pushed it, right? But this code doesn't do that. If you push the button halfway through the 1 second, the LED will only stay on for a half second since you last pushed it, not a whole second. Now, you could fault the user for that assumption, sure, but that's not very productive.

So let's rewrite this a little bit.

# Press To See Better

```C

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
int LEDcounter = 0;          // variable for storing how long the LED should be on

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // Set the LED counter to 1000
    LEDcounter = 1000;
  }
  
  if(LEDcounter != 0) {
    digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    LEDcounter--;
    if(LEDcounter == 0) {
        digitalWrite(ledPin, LOW);
    }
  }
}

```

This is /closer/ to what we want. While the button it pressed, the LEDcounter variable will be repeatedly set to 1000. If LEDcounter is non-zero, the LED gets turned on, and then the LEDcounter is decremented. If, after decrementing it, the LEDcounter variable is 0, the LED is turned off.

So now the LED will be on for 1000 "loops" after the button gets pressed. But how long is a loop? Nobody knows. The "loop" time is not related in any way to "human" time, but to however long it takes the processor to run all of the code in loop().

Arduino has a workaround for this, with the millis() function call, which returns the number of milliseconds(1/1000th of a second) that have passed since the program started running. Here's a version of the code that uses that to keep the LED on for 1 second:

# Press To See For 1 Second

```C

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
unsigned long LEDstartTime = 0;          // variable for storing the time that the LED was last turned on

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // Set the LED counter to 1000
    LEDstartTime = millis();
  }
  
  if( (millis() - LEDstartTime) < 1000) {
    digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  } else {
    digitalWrite(ledPin, LOW);
  }
}

```

So while the button is pressed, LEDstartTime is constantly being set to the current "time". If the current time minus the LEDstartTime is less than 1000 milliseconds, the LED is turned on, otherwise the LED is turned off.

So we can get back to "human" time, and still have it work how the user is expecting. So, no problem, right?

Kinda. This shows the tip of an underlying problem with Arduino. You see, embedded programming is mainly an exercise in resource management. Time is the main resource we need to worry about. Your processor has only a certain number of things it can do in a given time period, and it's up to you to make sure that it can do everything it needs to do in that time. But the way Arduino does things, time management is something you need either do in the foreground (delay(), which doesn't let you do anything besides sit there and wait) or work on constantly(Doing the ((millis() - LEDstartTime) < 1000) calculation/check isn't free!). Time management needs to be a first-class citizen, not an afterthought.

The core of the solution to this is already present in Arduino, it's just not exposed to where you can make good use of it. There has to be something, somewhere, keeping track of the passage of time, otherwise millis() would never change. There's other code running alongside the code you put in setup() and loop(), provided by the Arduino platform. So let's dig a little deeper. What calls our setup() and loop() functions?

# Peeling Back The Curtain

Main.cpp (Slightly abridged):
```C

#include <Arduino.h>

int main(void)
{
    init();
    
    setup();
    
    for (;;) {
        loop();
    }
        
    return 0;
}


```

Okay, so Main.cpp doesn't look too bad. main() is where C programs actually start running(...Usually. Now's not the time for that, though.). You can see that it calls init(), then it calls setup() (Hey, we know that function! That's what we normally get in our Arduino sketches.), and then it has a weird looking for loop that basically just means that it'll call loop() forever, which is the other function we normally see in Arduino sketches.

Still nothing here that keeps track of time, though... Well, the only thing we haven't seen in here is init(), so let's go find that.

wiring.c (Abridged):
```C

#include "wiring_private.h"

// the prescaler is set so that timer0 ticks every 64 clock cycles, and the
// the overflow handler is called every 256 ticks.
#define MICROSECONDS_PER_TIMER0_OVERFLOW (clockCyclesToMicroseconds(64 * 256))

// the whole number of milliseconds per timer0 overflow
#define MILLIS_INC (MICROSECONDS_PER_TIMER0_OVERFLOW / 1000)

// the fractional number of milliseconds per timer0 overflow. we shift right
// by three to fit these numbers into a byte. (for the clock speeds we care
// about - 8 and 16 MHz - this doesn't lose precision.)
#define FRACT_INC ((MICROSECONDS_PER_TIMER0_OVERFLOW % 1000) >> 3)
#define FRACT_MAX (1000 >> 3)

volatile unsigned long timer0_overflow_count = 0;
volatile unsigned long timer0_millis = 0;
static unsigned char timer0_fract = 0;

ISR(TIMER0_OVF_vect)
{
	// copy these to local variables so they can be stored in registers
	// (volatile variables must be read from memory on every access)
	unsigned long m = timer0_millis;
	unsigned char f = timer0_fract;

	m += MILLIS_INC;
	f += FRACT_INC;
	if (f >= FRACT_MAX) {
		f -= FRACT_MAX;
		m += 1;
	}

	timer0_fract = f;
	timer0_millis = m;
	timer0_overflow_count++;
}

unsigned long millis()
{
	unsigned long m;
	uint8_t oldSREG = SREG;

	// disable interrupts while we read timer0_millis or we might get an
	// inconsistent value (e.g. in the middle of a write to timer0_millis)
	cli();
	m = timer0_millis;
	SREG = oldSREG;

	return m;
}

void delay(unsigned long ms)
{
	uint32_t start = micros();

	while (ms > 0) {
		yield();
		while ( ms > 0 && (micros() - start) >= 1000) {
			ms--;
			start += 1000;
		}
	}
}

void init()
{
	// this needs to be called before setup() or some functions won't
	// work there
	sei();
	
	// on the ATmega168, timer 0 is also used for fast hardware pwm
	// (using phase-correct PWM would mean that timer 0 overflowed half as often
	// resulting in different millis() behavior on the ATmega8 and ATmega168)
	sbi(TCCR0A, WGM01);
	sbi(TCCR0A, WGM00);

	// set timer 0 prescale factor to 64
	// this combination is for the standard 168/328/1280/2560
	sbi(TCCR0B, CS01);
	sbi(TCCR0B, CS00);

	// enable timer 0 overflow interrupt
	sbi(TIMSK, TOIE0);
    
    // ************************************************
    // *                                              *
    // *   Izzy's note: I took out a chunk here for   *
    // *       simplicity. Don't worry about it.      *
    // *                                              *
    // ************************************************
}

```

Okay, so there's a /bit/ more going on in this file... We see our friends delay() and millis(), which we know what they do, even if we don't know how they work yet, so let's ignore those for the moment. That ISR(TIMER0_OVF_vect) thing has a scary name, so let's ignore it for a minute, too, and go look at init(), since that's what got us here.

This is where the Arduino code sets up the very basics of the hardware in your Arduino to do Arduino things. I've cut out a good chunk that we're not particularly interested in right now, so don't be surprised that it's so easy.

Basically what this chunk of code in init() is doing is setting up a piece of hardware on the processor called a "timer", which, much like the timers in your kitchen, are used to keep track of time. We're not particularly interested in exactly what period of time they're setting it up to measure, but it is good to know that this is what they're doing. The other thing this chunk of code does is enable the "overflow interrupt" for that timer. This is basically like the buzzer on your kitchen timer, that lets you know when the timer runs out.

That interrupt is what is actually being used to keep track of time. Every time the timer runs out, we know that a period of time has passed, and we just increment our counter accordingly. That's actually what's happening in that ISR(TIMER0_OVF_vect) function up near the top. When the interrupt gets triggered, that function gets called, and it says "hey, increment the counter a bit", then goes back to whatever your code was doing.

So how does knowing how Arduino keeps track of time help? Well, wouldn't it be nice if we could just have another function in our sketch that gets called every time a millisecond passes? Then we could just put all of our code that deals with time in that function, and even just use normal counters to keep track of when our LED should turn off. Something like this:

# Into The Theoretical

```C

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
unsigned long LEDonTimerMS = 0;          // variable for storing the time that the LED was last turned on

ISR(TIMER0_OVF_vect)
{
	// copy these to local variables so they can be stored in registers
	// (volatile variables must be read from memory on every access)
	unsigned long m = timer0_millis;
	unsigned char f = timer0_fract;

	m += MILLIS_INC;
	f += FRACT_INC;
	if (f >= FRACT_MAX) {
		f -= FRACT_MAX;
		m += 1;
        tick1ms();
	}

	timer0_fract = f;
	timer0_millis = m;
	timer0_overflow_count++;
}

void tick1ms() {
    if(LEDonTimerMS) {
        LEDonTimerMS--;
        if(!LEDonTimerMS) {
            digitalWrite(ledPin, LOW);
        }
    }
}

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // Set the LED counter to 1000
    digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    LEDonTimerMS = 1000;
  }
}

```

Note that this will no longer compile in Arduino. We're reusing the ISR(TIMER0_OVF_vect) function name, which gets the compiler mad at us. So this is where we have to start using a bit of imagination.

I changed ISR(TIMER0_OVF_vect) a little bit to call tick1ms() every time the millis() result gets incremented.

Now while the button is being pressed, the timer gets set to 1000 milliseconds, and then it gets decremented in tick1ms(), and the LED turns on and off exactly as expected, and we don't have any complicated checks happening. Even if you use a delay() in loop(), the LED will still turn off at the right time. Isn't that nice? This is /almost/ exactly what we want.

There's still a problem with delay(), though... What if we have something that's not necessarily time-based, so it doesn't belong in tick1ms(), but it's something that could be happening at any time, so we don't want it in loop() where a rogue delay() could keep it from happening?

Something like, say, waiting for a motor to move something to the end of a screw. We don't necessarily know how long it will take, so handling it in tick1ms() isn't quite right, but we don't want to leave the motor running after it reaches the end, either, because that could damage the motor.

This is also fairly easily fixed, if we could change the Arduino internals just a bit more. So let's take a look.

# The Final Fantasy

```C

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int motorStopPin = 3;     // the number of the pin for the button at the end of the motor's screw
const int ledPin =  13;      // the number of the LED pin
const int motorRunPin = 14; // the number of the motor's control pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
unsigned long LEDonTimerMS = 0;          // variable for storing the time that the LED was last turned on

ISR(TIMER0_OVF_vect)
{
	// copy these to local variables so they can be stored in registers
	// (volatile variables must be read from memory on every access)
	unsigned long m = timer0_millis;
	unsigned char f = timer0_fract;

	m += MILLIS_INC;
	f += FRACT_INC;
	if (f >= FRACT_MAX) {
		f -= FRACT_MAX;
		m += 1;
        tick1ms();
	}

	timer0_fract = f;
	timer0_millis = m;
	timer0_overflow_count++;
}

void tick1ms() {
    if(LEDonTimerMS) {
        LEDonTimerMS--;
        if(!LEDonTimerMS) {
            digitalWrite(ledPin, LOW);
        }
    }
}

void delay(unsigned long ms)
{
	uint32_t start = micros();

	while (ms > 0) {
		yield();
		while ( ms > 0 && (micros() - start) >= 1000) {
			ms--;
			start += 1000;
            idle();
		}
	}
}

int main(void)
{
    init();
    
    setup();
    
    for (;;) {
        idle();
        loop();
    }
        
    return 0;
}

void idle() {
    motorStopState = digitalRead(motorStopPin);
    
    if(motorStopState == HIGH) {
        digitalWrite(motorRunPin, LOW);
    }
}

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  pinMode(motorRunPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  pinMode(motorStopPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // Set the LED counter to 1000
    digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    LEDonTimerMS = 1000;
    digitalWrite(motorRunPin, HIGH);
  }
}

```

There. Two little modifications to functions we've already seen, to have delay() call idle() while we wait, and add a call to idle() in the for loop in main(), too, so that things in idle() happen whether we're delay()ing or not. Also made pressing the button turn on the motor in loop(), too.

So now we have four functions that can handle almost any combination of time management we need. tick() happens at regular intervals, idle() happens nearly constantly in the background, setup() happens once at the beginning of the program to set things up, and loop() is our normal "game" code, where the majority of the user-facing stuff should happen.

Both tick() and idle() have the restriction that you should NEVER call delay() in them. These two functions should happen as fast as possible. If tick() takes more than a millisecond, for example, your LED code falls apart again. If you delay() in idle(), the delay() function itself will call idle(), and then you're caught in an infinite loop. But this is fairly easy to work with, because usually you're only going to be doing a little bit of work at a given interval anyways. I'll have more examples of this later.

Unfortunately, we've already moved past where Arduino can support us. So the next step is figuring out how to move on to a system that's more flexible. Stay tuned.